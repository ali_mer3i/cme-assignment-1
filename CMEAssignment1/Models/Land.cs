﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMEAssignment1.Models
{
    class Land: Property
    {
        private double _area { get; set; }
        private bool _canBeFaremd { get; set; }

        public Land(String title, int area, String address, bool canBeFarmed) {
            setID();
            this.Title = title;
            this._area = area;
            this.Address = address;
            this._canBeFaremd = canBeFarmed;
            this.Price = 3000 * area;
        }
        override public void DisplayProperty()
        {
            base.DisplayProperty();
            String toDisplay = string.Format(" area : {0} m2 /n can be farmed: {1} ", this._area, this._canBeFaremd? "Yes" : "No");
            Console.WriteLine(toDisplay.Replace("/n", Environment.NewLine));
            Console.WriteLine();
        }
    }
}
