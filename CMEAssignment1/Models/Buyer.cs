﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CMEAssignment1.Models
{
    class Buyer
    {
        static int nextId = 0;

        private int _id { get; set; }
        private string _fullName { get; set; }
        private double _credit { get; set; }
        private List<int> ownedPropertiesIds { get; set; }

        public Buyer(String fullName, double credit)
        {
            this._id = Interlocked.Increment(ref nextId);
            this._fullName = fullName;
            this._credit = credit;
            this.ownedPropertiesIds = new List<int>();

        }

        public void DisplayBuyer()
        {
            String toDisplay = string.Format(" Buyer: {0} /n Nb of Owned Properties: {1} /n Ramaining Credit: {2}", this._fullName, this.ownedPropertiesIds.Count, this._credit);
            Console.WriteLine(toDisplay.Replace("/n", Environment.NewLine));
            Console.WriteLine();
        }

        public bool BuyProperty(Property property)
        {
            if (property.Price <= this._credit && property.OwnerId == null)
            {
                this.ownedPropertiesIds.Add(property.Id);
                property.OwnerId = this._id;
                this._credit -= property.Price;

                Console.WriteLine("----- Buyer Purchased ------");
                String toDisplay = string.Format("{0} with ID {1} was purchased by {2} for {3}", property.GetType().ToString().Split(".".ToCharArray())[2], property.Id, this._fullName, property.Price);
                Console.WriteLine(toDisplay);
                Console.WriteLine();
                return true;
            }
            return false;
        }
    }
}
