﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMEAssignment1.Models
{
    public abstract class Property
    {
        static int nextId = 0;

        public int Id { get; set; }
        public String Title { get; set; }
        public int Price { get; set; }
        public String Address { get; set; }
        public int? OwnerId { get; set; }

        public void setID()
        {
          Id =  Interlocked.Increment(ref nextId);

        }

        public virtual void DisplayProperty() {
            var toDisplay = string.Format(" id : {0} /n title: {1} /n price: {2} /n address: {3}", this.Id, this.Title, this.Price, this.Address);
            Console.WriteLine(toDisplay.Replace("/n",Environment.NewLine));
        }

        public static void removePropertyRandom(List<Property> properties) {
            var random = new Random();
            int randNumber = random.Next(1, properties.Count - 1);
            if (properties[randNumber].OwnerId == null)
            {
                Console.WriteLine("removed property with id {0}", properties[randNumber].Id);
                properties.RemoveAt(randNumber);

            }
            else {
                Console.WriteLine("cannot remove property with id {0} since purchased", properties[randNumber].Id);
            }
            Console.WriteLine();

        }

    }
    
}
