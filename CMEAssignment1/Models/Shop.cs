﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMEAssignment1.Models
{
    class Shop: Property
    {
        private double _area { get; set; }
        private int _business { get; set; }

        public Shop(String title, int area, String address, int business)
        {
            setID();
            this.Title = title;
            this._area = area;
            this.Address = address;
            this._business = business;
            if (area >50) {
                this.Price = 120000;
            }
            else {
                this.Price = 80000;
            }
            this.Price = 3000 * area;
        }

        override public void  DisplayProperty()
        {
            base.DisplayProperty();
            BusinessType bType = (BusinessType)this._business;
            String toDisplay = string.Format(" area : {0} m2 /n busness: {1} ", this._area,bType.ToString());
            Console.WriteLine(toDisplay.Replace("/n", Environment.NewLine));
            Console.WriteLine();
        }
    }

    

    public enum BusinessType {
        Food = 1,
        Repair = 2,
        Retail = 3
    }
}
