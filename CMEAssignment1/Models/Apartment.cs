﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMEAssignment1.Models
{
    class Apartment: Property
    {
        private int _nbOfRooms { get; set; }

        public Apartment(String title, int nbOfRooms, String address) {
            setID();
            this.Title = title;
            this._nbOfRooms = nbOfRooms;
            this.Address = address;
            this.Price = 15000 * nbOfRooms;
        }
        override public void DisplayProperty()
        {
            base.DisplayProperty();
            String toDisplay = string.Format(" number of rooms : {0} ", this._nbOfRooms);
            Console.WriteLine(toDisplay.Replace("/n", Environment.NewLine));
            Console.WriteLine();
        }

    }
}
