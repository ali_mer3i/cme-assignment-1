﻿using CMEAssignment1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMEAssignment1
{
    class Program
    {
        public static List<Property> properties = new List<Property>();
        public static List<Buyer> buyers = new List<Buyer>();

        static void Main(string[] args)
        {
            properties.AddRange(new List<Apartment>(new Apartment[]{
                                    new Apartment("Appartment 1", 1, "Beirut, lebanon, hamra street"),
                                    new Apartment("Appartment 2", 2, "Beirut, lebanon, Jamous street"),
                                    new Apartment("Appartment 3", 3, "Beirut, lebanon, Salim Slem"),
                                    new Apartment("Appartment 4", 4, "South, lebanon, Kafra"),
                                    new Apartment("Appartment 5", 5, "Baabda, lebanon, unknown street")

            }));

            properties.AddRange(new List<Land>(new Land[]{
                                    new Land("Land 1", 2, "Lebanon, Kafra", true),
                                    new Land("Land 2", 20, "Lebanon, Tebnin", false)

            }));

            properties.AddRange(new List<Shop>(new Shop[]{
                                    new Shop("shop 1", 10, "Lebanon, Beirut, Jamous Street", (int)BusinessType.Food),
                                    new Shop("shop 2", 40, "Lebanon, Beirut, Hadath ", (int)BusinessType.Repair),
                                    new Shop("shop 3", 60, "Lebanon, Beirut, Gallery Semaan", (int)BusinessType.Retail)

            }));

            buyers.AddRange(new List<Buyer>(new Buyer[]{
                                    new Buyer("Buyer 1", 60000),
                                    new Buyer("Buyer 2", 10000),
                                    new Buyer("Buyer 3", 400000)

            }));

            printDescription("Display All Properties");

            foreach (var property in properties)
            {
                property.DisplayProperty();
            }
            printSeperator();

            printDescription("Display Land Properties");

            foreach (var property in properties)
            {
                if (property.GetType() == typeof(Land))
                {
                    property.DisplayProperty();
                }
            }
            printSeperator();

            printDescription("Display Land Properties between 45, 000 and 100, 000");

            List<Property> pricedProperties = properties.Where(x => x.Price > 45000 && x.Price < 100000).ToList();

            foreach (var property in pricedProperties)
            {
                property.DisplayProperty();
            }
            printSeperator();

            //buy property for first buyer
            foreach (var buyer in buyers)
            {
                foreach (var property in properties)
                {
                    if (buyer.BuyProperty(property))
                        break;
                }

            }

            printSeperator();
            foreach (var buyer in buyers)
            {
                buyer.DisplayBuyer();
            }
            printSeperator();

            printDescription("Display Property with id 2");

            var property2 = properties.Where(x => x.Id == 2).First();
            property2.Title = "Modified";
            property2.DisplayProperty();
            printSeperator();

            printDescription("Attempt to remove 2 properties");
            Property.removePropertyRandom(properties);
            Thread.Sleep(100);
            Property.removePropertyRandom(properties);
            printSeperator();

            printDescription("Display All Properties");

            foreach (var property in properties)
            {
                property.DisplayProperty();
            }
            printSeperator();

        }

        private static void printDescription(string message)
        {
            Console.WriteLine("-----------------" + message + "------------------");
        }

        private static void printSeperator()
        {
            Console.WriteLine("-----------------------------------");
        }
    }
}
